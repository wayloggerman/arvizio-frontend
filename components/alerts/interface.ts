


export enum AlertType {
    success = 'success',
    error = 'error',
    warning = 'warning',
    info = 'info'
}

export interface IAlert {
    type: AlertType
    message: string
    timeout?: number
}

export interface AlertBase {
    alert: IAlert | null
}
