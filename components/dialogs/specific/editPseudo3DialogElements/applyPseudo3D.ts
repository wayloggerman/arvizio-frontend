import * as nanoid from 'nanoid'
import { getFile } from '~/plugins/DataMounter'
import { Media } from '~/plugins/interfaces'

export async function applyPseudo3D(
  context: any,
  media: Media,
  targetArray: {
    images: any[]
    key: string
  }[],
  trackingArray: any[] = []
) {
  const files = context.$store.getters['data/file/all']

  if (!files || files.length === 0) {
    return
  }

  const currentFiles = files.filter(
    (file) => file.media.id === media.id && file.type !== 'audio'
  )

  currentFiles.forEach((file) => {
    const { settings } = file

    if (!settings) {
      trackingArray.push({
        id: file.id,
        relativeLine: 0,
        relativeColumn: 0,
      })
    }
    if (settings) {
      trackingArray.push({
        id: file.id,
        relativeLine: settings.relativeLine ?? 0,
        relativeColumn: settings.relativeColumn ?? 0,
      })
    }
  })

  const numberOfLines =
    typeof media.settings === 'object'
      ? media.settings.numberOfLines ?? 1
      : JSON.parse(media.settings).numberOfLines ?? 1

  for (let i = 0; i < numberOfLines; i++) {
    targetArray.push({
      key: nanoid.nanoid(),
      images: [],
    })
  }

  for (const file of currentFiles) {
    const src = await getFile(context, file.media.id, file.id)
    const settings =
      typeof file.settings === 'object'
        ? file.settings
        : JSON.parse(file.settings)

    const relativeLine = settings?.relativeLine ?? 0
    const relativeColumn = settings?.relativeColumn ?? 0

    targetArray[relativeLine].images.push({
      src,
      name: file.name,
      id: file.id,
      pos: relativeColumn,
    })
  }

  for (const list of targetArray) {
    list.images = list.images.sort((a, b) => {
      if (a.pos === undefined) return 1
      if (b.pos === undefined) return -1
      return a.pos - b.pos
    })
  }
}
