


export abstract class InputWithStats {

    success: boolean = false
    error: boolean = false

    abstract valid(value: string): boolean
    abstract invalid(value: string): string
    EmitFunc(): any { }


    rules = [(value: string) => this.check(value)]

    changeValidationState(opt: { success?: boolean; error?: boolean }) {
        if (opt.success) {
            this.success = true
            this.error = false
            return
        }
        if (opt.error) {
            this.success = false
            this.error = true
            return
        }
        this.success = false
        this.error = false
    }
    check(value: string): boolean | string {
        if (!value.length) {
            this.changeValidationState({ success: false, error: false })
            return true
        }
        if (this.valid(value)) {
            this.changeValidationState({ success: true })
            this.EmitFunc()
            return true
        }
        this.changeValidationState({ error: true })
        return this.invalid(value)
    }
}
