import * as THREE from './three.module.js'

import Stats from './stats.module.js'

import { OrbitControls } from './OrbitControls.js'
import { FBXLoader } from './FBXLoader.js'

let camera, scene, renderer, stats

const clock = new THREE.Clock()

let mixer

export async function mountFBXPlayer(container: HTMLElement, path: string) {
  const width = container.clientWidth
  const height = container.clientHeight
  camera = new THREE.PerspectiveCamera(45, width / height, 1, 2000)
  camera.position.set(50, 50, 50)

  scene = new THREE.Scene()
  scene.background = new THREE.Color(0xa0a0a0)
  // scene.fog = new THREE.Fog(0xa0a0a0, 200, 1000)

  const hemiLight = new THREE.HemisphereLight(0xffffff, 0x444444)
  ;(hemiLight as any).position.set(20, 200, 0)
  scene.add(hemiLight)

  const dirLight = new THREE.DirectionalLight(0xffffff)
  dirLight.castShadow = true
  dirLight.shadow.camera.top = 180
  dirLight.shadow.camera.bottom = -100
  dirLight.shadow.camera.left = 120
  dirLight.shadow.camera.right = 120
  scene.add(dirLight)

  const mesh = new THREE.Mesh(
    new THREE.PlaneGeometry(20000, 20000),
    new THREE.MeshPhongMaterial({ color: 0x999999, depthWrite: false }) as any
  )
  ;(mesh as any).rotation.x = -Math.PI / 2
  mesh.receiveShadow = true
  scene.add(mesh)

  const grid = new THREE.GridHelper(20000, 20000, 0x000000, 0x000000)
  grid.material.opacity = 0.2
  grid.material.transparent = true
  scene.add(grid)

  // model
  const loader = new FBXLoader()
  loader.setWithCredentials(true)
  loader.withCredentials = true
  let isError = false

  renderer = new THREE.WebGLRenderer({ antialias: true })
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.shadowMap.enabled = true

  const childs = Array.from(container.children)
  for (const child of childs) child.remove()
  container.appendChild(renderer.domElement)

  const controls = new OrbitControls(camera, renderer.domElement)
  controls.target.set(1, 35, 5)
  controls.update()

  window.addEventListener('resize', onWindowResize)
  const object = loader.load(path, (object) => {
    object.scale.multiplyScalar(0.1)
    if (object.animations.length > 0) {
      mixer = new THREE.AnimationMixer(object)
      const action = mixer.clipAction(object.animations[0])
      action.play()
    }

    scene.add(object)
    object.traverse(function (child) {
      if (child.isMesh) {
        child.castShadow = true
        child.receiveShadow = true
      }
    })
  })

  if (isError) {
    return false
  }

  return true
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()
}

export function animate() {
  requestAnimationFrame(animate)

  const delta = clock.getDelta()

  if (mixer) mixer.update(delta)

  renderer.render(scene, camera)
}
