import * as Panolens from 'panolens'

interface createSpotDto {
  vector: Panolens.Vector3
  inx: number
  toImageName: string
}

export function getSpot(dto: createSpotDto) {
  const { vector } = dto

  const point = new Panolens.Infospot(80, Panolens.DataImage.Arrow)
  point.addHoverText(dto.toImageName)
  point.position.set(vector.x, vector.y, -1000)

  return point
}
