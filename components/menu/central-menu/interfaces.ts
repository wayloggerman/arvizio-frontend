export enum CentralMenuItemType {
  x360 = 'deg360',
  gallery = 'gallery',
  model3d = 'model3d',
  pseudo3d = 'pseudo3d',
}

export const CentralMenuItemFileExt = {
  [CentralMenuItemType.x360]: '.jpg .jpeg .png',
  [CentralMenuItemType.gallery]: '.jpg .jpeg .png .mp3, .mp4',
  [CentralMenuItemType.pseudo3d]: '.jpg .jpeg .png .mp3',
  [CentralMenuItemType.model3d]: '.fbx',
}

export const CentralMenuItemLabel = {
  [CentralMenuItemType.model3d]: '3D модель',
  [CentralMenuItemType.x360]: '360°',
  [CentralMenuItemType.gallery]: 'Галерея',
  [CentralMenuItemType.pseudo3d]: 'Псевдо 3D',
}
export interface CentralMenuItem {
  type: CentralMenuItemType
}
