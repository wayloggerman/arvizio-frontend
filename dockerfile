FROM node:16-alpine
COPY . .
RUN apk update && apk upgrade
RUN apk add python3
RUN apk add git
RUN npm i
RUN npm run build
ENV backend=https://185.251.88.184:3333 
CMD ["npm", "start"]


