import { Context } from '@nuxt/types'
import UserState from '~/store/data/user'

export default async function auth(ctx: any) {
  const { store, redirect, app, route } = ctx
  await UserState.mount({
    $store: store,
  })
  const user = store.getters['data/user/all']
  if (user.length === 0) {
    redirect('/')
  }
}
