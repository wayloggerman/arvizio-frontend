import project from '~/store/data/project'
import media, { MediaType } from '~/store/data/media'
import file from '~/store/data/file'
import spot from '~/store/data/spot'

export async function MountData(context: any) {
  context.$store.dispatch('App/startLoading')

  await project.mount(context)
  await media.mount(context)
  await file.mount(context)
  await spot.mount(context)

  context.$store.dispatch('App/stopLoading')
}

export async function ForceMountData(
  context: any,
  shared?: { projectId: number }
) {
  context.$store.dispatch('App/startLoading')

  if (shared) {
    await mountShared(context, shared.projectId)
    return
  }

  await project.mount(context, true)
  await media.mount(context, true)
  await file.mount(context, true)
  await spot.mount(context, true)

  context.$store.dispatch('App/stopLoading')
}

async function mountShared(context: any, projectId: number) {
  await project.mountShared(context, projectId)
  await media.mountShared(context, projectId)
  await file.mountShared(context, projectId)
  await spot.mountShared(context, projectId)
}

export function getLogo(context: any, projectId: number) {
  const logoMedia = context.$store.getters['data/media/all'].filter(
    (m: any) => m.project.id === projectId && m.type === MediaType.logo
  )

  if (!logoMedia.length) return null

  const logoFile = context.$store.getters['data/file/all'].filter(
    (f: any) => f.media.id === logoMedia[0].id
  )

  if (!logoFile.length) return null

  const id = logoFile[0].id
  const l = file.download(context, logoMedia[0].id, id)

  return l
}

export function getFile(context: any, mediaId: number, fileId: number) {
  const f = file.download(context, mediaId, fileId)
  return f
}
