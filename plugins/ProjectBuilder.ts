import { transliterate as tr, slugify } from 'transliteration'
import { ICreateProject, ProjectType } from '../store/data/project'
import project from '../store/data/project'
import media, { MediaType } from '../store/data/media'

import { ICreateMedia } from '../store/data/media'
import file, { ICreateFile, IUploadFile } from '../store/data/file'

export interface IHandleMedia {
  mediaName: string
  mediaType: MediaType
  mediaDescription: string
  files: File[] | File
  filesType: string
}
export type MediaId = number
export default class ProjectBuilder {
  private async createProject(dto: ICreateProject) {
    return project.create(this.context, dto, false)
  }
  private async createMedia(dto: ICreateMedia) {
    return media.create(this.context, dto, false)
  }
  private async createFile(dto: ICreateFile) {
    return file.create(this.context, dto, false)
  }
  private async uploadFile(dto: IUploadFile) {
    return file.upload(this.context, dto)
  }

  public async createMediaFromFile(
    dto: IHandleMedia,
    projectId: number
  ): Promise<MediaId | boolean> {
    if (Array.isArray(dto.files)) return false

    const file = dto.files

    if (!file) {
      return false
    }
    const media = await this.createMedia({
      name: dto.mediaName,
      projectId,
      type: dto.mediaType,
      description: dto.mediaDescription,
    })
    const fileId = await this.createFile({
      name: file.name,
      mediaId: media,
      type: dto.filesType,
    })
    await this.uploadFile({
      file: file,
      fileId,
      mediaId: media,
      fileName: file.name,
      type: dto.filesType,
    })

    return media
  }

  private async handleMedia(
    dto: IHandleMedia,
    projectId: number,
    parentMediaId?: number
  ) {
    try {
      if (Array.isArray(dto.files))
        for (const file of dto.files) {
          const mediaId =
            parentMediaId && parentMediaId > 0
              ? parentMediaId
              : await this.createMedia({
                  name: dto.mediaName,
                  projectId,
                  type: dto.mediaType,
                  description: dto.mediaDescription,
                })

          const fileId = await this.createFile({
            name: file.name,
            mediaId,
            type: dto.filesType,
          })

          await this.uploadFile({
            file: file,
            fileId,
            mediaId,
            fileName: file.name,
            type: dto.filesType,
          })
        }

      return true
    } catch (e) {
      console.log(e)

      return false
    }
  }

  constructor(private context: any) {}

  private getImageType(type: ProjectType) {
    switch (type) {
      case ProjectType.pano:
        return MediaType.panorama
      case ProjectType.dem3:
        return MediaType.image
      case ProjectType.pseudo3d:
        return MediaType.pseudo3d
      case ProjectType.video:
        return MediaType.video
      case ProjectType.audio:
        return MediaType.audio
      case ProjectType.gallery:
        return MediaType.pseudo3d
    }
  }

  async create(dto: ICreateProject) {
    try {
      const projectId = await this.createProject(dto)

      if (!projectId) {
        return null
      }
      const logo = dto.logo
      if (logo) {
        await this.handleMedia(
          {
            mediaName: 'logo_of_project',
            mediaType: MediaType.logo,
            mediaDescription: 'logo of project',
            files: [logo],
            filesType: 'image',
          },
          projectId
        )
      }

      const fbxImages = dto.images.filter((image) =>
        image.name?.toLocaleLowerCase().includes('.fbx')
      )
      const images = dto.images.filter(
        (image) => !image.name?.toLocaleLowerCase().includes('.fbx')
      )
      this.handleMedia(
        {
          files: images,
          mediaDescription: 'image of project',
          mediaName: 'image_of_project',
          mediaType: this.getImageType(dto.type),
          filesType: 'image',
        },
        projectId
      )

      this.handleMedia(
        {
          files: fbxImages,
          mediaDescription: '3d image of project',
          mediaName: '3d image_of_project',
          mediaType: MediaType.model3d,
          filesType: 'model3d',
        },
        projectId
      )
      this.handleMedia(
        {
          files: dto.videos,
          mediaDescription: 'video of project',
          mediaName: 'video_of_project',
          mediaType: MediaType.video,
          filesType: 'video',
        },
        projectId
      )

      this.handleMedia(
        {
          files: dto.audios,
          mediaDescription: 'audio of project',
          mediaName: 'audio_of_project',
          mediaType: MediaType.audio,
          filesType: 'audio',
        },
        projectId
      )
      return true
    } catch (e) {
      return false
    }
  }

  static async create(context: any, dto: ICreateProject) {
    try {
      context.$store.dispatch('App/startLoading')
      const res = await new ProjectBuilder(context).create(dto)
      context.$store.dispatch('App/stopLoading')
      context.$store.dispatch('view/dialog/close')

      context.$store.dispatch(
        'view/alert/setAlert',
        res
          ? {
              type: 'success',
              message:
                'Проект успешно создан, вы будете перенаправлены в библиотеку',
            }
          : {
              type: 'error',
              message: 'Произошла ошибка при создании проекта',
            }
      )

      if (res) {
        setTimeout(() => {
          context.$router.push({ name: 'library' })
        }, 1000)
      }
    } catch (e) {}
  }

  static handleMedia(
    context: any,
    dto: IHandleMedia,
    projectId: number,
    parentMediaId?: number
  ) {
    return new ProjectBuilder(context).handleMedia(
      dto,
      projectId,
      parentMediaId
    )
  }

  static createMediaFromFile(
    context: any,
    dto: IHandleMedia,
    projectId: number
  ) {
    return new ProjectBuilder(context).createMediaFromFile(dto, projectId)
  }
}
