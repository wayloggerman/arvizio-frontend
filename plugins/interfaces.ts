import { IAlert } from '~/components/alerts/interface'
import { CentralMenuItemType } from '~/components/menu/central-menu/interfaces'
import * as PANOLENS from 'panolens'
import * as THREE from 'three'
export interface AppState {
  loading: boolean
}

export interface setCoordDTO {
  vector: Vector3D
  imageInx: number
  spotInx: number
}
export interface UserState {
  email: string | undefined
  id: number | undefined
  avatar: string | undefined
}

export interface SessionState {
  token: string | undefined
}

export enum DialogMode {
  signIn = 'signIn',
  signUp = 'signUp',
  forgotPassword = 'forgotPassword',
  createProject = 'createProject',
  model3d = 'model3d',
  model3dType = 'model3dType',
  model360 = 'model360',
  gallery = 'gallery',
  addMediaToGallery = 'addMediaToGallery',
  editPsuedo3d = 'editPsuedo3d',
  gallery360 = 'gallery360',
  galleryComplex = 'galleryComplex',
}

export interface DialogState {
  open: boolean
  mode: DialogMode | undefined
}

export enum indexChoise {
  library = 'library',
  newProject = 'newProject',
  default = 'default',
}
export interface IndexPageState {
  choise: indexChoise
}
export interface Project {
  id: number
  name: string
  description: string
  type: string
  createdAt: string
  updatedAt: string
  deletedAt: string
  public: false
}

export interface Media {
  id: number
  type: string
  name: string
  createdAt: string
  updatedAt: string
  deletedAt: string
  size: number
  description: string
  project: Project
  order: number
  settings: any
}
export interface MediaFile {
  id: number
  name: string
  path: string
  size: number
  storage: string
  createdAt: string
  updatedAt: string
  deletedAt: string
  extension: string
  media: Media
  type: string
}

export enum ProjectOptions {
  external = 'external',
  internal = 'internal',
  panorama = 'panorama',
  other = 'other',
}

export const ProjectOptionLabels = {
  [ProjectOptions.external]: 'Внешняя модель',
  [ProjectOptions.internal]: 'Внутренняя модель',
  [ProjectOptions.panorama]: 'Панорамные изображения (360°)',
  [ProjectOptions.other]: 'Другие медиа файлы',
}

export interface AlertState {
  alert: IAlert | undefined
}

export interface CreateProjectState {
  name: string
  logo: File | undefined
  images: File[]
  audio: File | undefined
  parentSite: string
  backLinkToParentSite: string
  type: CentralMenuItemType | undefined
  option?: ProjectOptions
}

export interface editorState {
  project: IProjectSchemaBackend | undefined
  images: ProjectFile[]
  currentGroupInx: number
  buffers: Buffer[]
  drawBufferInx: number
  logoBuffer: Buffer | undefined
  newSpot: {
    from: string
    to: string
    inx: number | undefined
    vector: {
      x: number
      y: number
      z: number
    }
  }
}

// backend types
export enum ProjectTypeBackend {
  deg360 = 'deg360',
  model3d = 'model3d',
  gallery = 'gallery',
}

export enum FileTypeBackend {
  img = 'img',
  video = 'video',
  logo = 'logo',
  audio = 'audio',
}

export enum ProjectStorageBackend {
  local = 'local',
  sftp = 'sftp',
  ftp = 'ftp',
  smb = 'smb',
  webdav = 'webdav',
  s3 = 's3',
  git = 'git',
  github = 'github',
  gitlab = 'gitlab',
  bitbucket = 'bitbucket',
  dropbox = 'dropbox',
}

export interface Vector3D {
  x: number
  y: number
  z: number
}
export interface Spot {
  from: string
  to: string
  vector: Vector3D
}

export enum FileType {
  img = 'img',
  video = 'video',
  logo = 'logo',
  audio = 'audio',
}

export interface ProjectFile {
  name: string
  path: string
  type: FileType
  mime: string
  spots: Spot[]
}
export interface ProjectSettingsBackend {
  parentSite: string
  textParentSite: string
  deleted: boolean
  deletedAt: string
  files: ProjectFile[]
  groupFiles: {
    groupKey: string
    files: ProjectFile[]
  }[]
  spots: Spot[]
}

export const defaultSettings: ProjectSettingsBackend = {
  parentSite: '',
  textParentSite: '',
  deleted: false,
  deletedAt: '',
  files: [],
  groupFiles: [],
  spots: [],
}

export interface IProjectSchemaBackend {
  id?: number
  name?: string
  type?: ProjectTypeBackend
  owner_id?: number
  size_in_bytes?: string
  link_to_read?: string
  link_to_change?: string
  free_to_change?: boolean
  created_at?: Date
  updated_at?: Date
  storage?: ProjectStorageBackend
  settings?: ProjectSettingsBackend
  deleted?: boolean
  deleted_at?: Date
}
