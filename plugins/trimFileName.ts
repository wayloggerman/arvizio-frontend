export function trimFileName(
  fileName: string,
  extension: string,
  length: number = 15
) {
  const startLength = fileName.length
  const nameWithouExt = fileName.replace(extension, '')
  if (startLength >= length) {
    const semiLength = Math.floor(length / 2)
    return (
      nameWithouExt.slice(0, semiLength) +
      '...' +
      nameWithouExt.slice(
        nameWithouExt.length - semiLength,
        nameWithouExt.length
      ) +
      extension
    )
  }
  return nameWithouExt + extension
}
