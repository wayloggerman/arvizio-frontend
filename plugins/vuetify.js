import vk from '~/components/icons/vk.vue'
import mail from '~/components/icons/mail.vue'
import google from '~/components/icons/google.vue'
import yandex from '~/components/icons/yandex.vue'

export default {
    icons: {
        values: {
            vk: {
                component: vk
            },
            mail: {
                component: mail
            },
            google: {
                component: google
            },
            yandex: {
                component: yandex
            }
        }
    }
}
