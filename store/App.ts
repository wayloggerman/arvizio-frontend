import { AppState } from '../plugins/interfaces';



type State = AppState


function state(): State {
    return {
        loading: false
    }
}

const getters = {
    loading: (state: State) => state.loading
}
const mutations = {
    startLoading: (state: State) => state.loading = true,
    stopLoading: (state: State) => state.loading = false,

    loading: (state: State, loading: boolean) => state.loading = loading
}
const actions = {
    startLoading({ commit }: any) {
        commit('startLoading')
    },
    stopLoading({ commit }: any) {
        commit('stopLoading')
    },
    loading({ commit }: any, loading: boolean) {
        commit('loading', loading)
    }

}

export default {
    state,
    getters,
    actions,
    mutations
}
