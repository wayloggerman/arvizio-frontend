import translitiration from 'transliteration'
import { MediaId } from '~/plugins/ProjectBuilder'
import { removeUndefines } from '~/plugins/shared'
interface BranchState {
  entities: Entity[]
}
export enum Storage {
  LOCAL = 'local',
  S3 = 's3',
  GCS = 'gcs',
  Yandex = 'yandex',
}

type State = BranchState
interface FileSettings {
  [key: string]: any
  relativeColumn: number
  relativeLine: number
}
type Entity = {
  id: number
  media: number
  name: string
  size: number
  storage: Storage
  settings: FileSettings
}

export interface ICreateFile {
  name: string
  mediaId: number
  type: string
}

export type FileId = number
export interface IUploadFile {
  file: File
  fileId: FileId
  fileName: string
  mediaId: MediaId
  type: string
}

const BranchName = 'data/file'

async function mount(context: any, force: boolean = false) {
  if (force === false && context.$store.getters[`${BranchName}/all`].length)
    return
  await context.$store.dispatch(`${BranchName}/fetch`)
}

async function mountShared(context: any, projectId: number) {
  await context.$store.dispatch(`data/file/fetchByProjectId`, projectId)
}

async function create(
  context: any,
  entity: ICreateFile,
  effects: boolean = true
) {
  if (effects) context.$store.dispatch('App/startLoading')

  const res = await context.$store.dispatch(
    'data/file/create',
    removeUndefines(entity)
  )
  if (effects) {
    context.$store.dispatch('App/stopLoading')
    context.$store.dispatch('view/dialog/close')
  }
  return res
}

async function deleteFile(context: any, fileId: number) {
  await context.$store.dispatch('data/file/delete', fileId)
}

async function upload(context: any, entity: IUploadFile) {
  const res = await context.$store.dispatch(
    'data/file/upload',
    removeUndefines(entity)
  )
  return res
}

function download(context: any, mediaId: number, fileId: number) {
  return context.$store.dispatch('data/file/download', {
    mediaId,
    fileId,
  })
}

export async function updateFileSettings(
  context: any,
  dto: {
    fileId: FileId
    settings: Partial<FileSettings>
  }
) {
  const res = await context.$store.dispatch('data/file/updateSettings', dto)
  return res
}

const state: State = {
  entities: [],
}

const getters = {
  all(state: BranchState) {
    return state.entities
  },
  byMediaId: (state: BranchState) => (id: number) => {
    return state.entities.filter(
      (e) => (e.media as never as { id: number }).id === id
    )
  },
  mediaByName: (state: BranchState) => (name: string) => {
    return state.entities.find((e) => e.name === name)?.media
  },
}

const actions = {
  async updateSettings(
    { commit }: any,
    dto: {
      fileId: FileId
      settings: Partial<FileSettings>
    }
  ) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const { $axios } = ctx
      const res = await $axios.$patch(
        `${backend}/file`,
        {
          id: dto.fileId,
          settings: JSON.stringify(dto.settings),
        },
        {
          withCredentials: true,
        }
      )

      commit('updateSettings', dto)
      return true
    } catch (e) {
      return false
    }
  },
  download(
    { commit }: any,
    dto: {
      mediaId: number
      fileId: number
    }
  ): string | undefined {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      return `${backend}/download/${dto.mediaId}/${dto.fileId}`
    } catch (e) {
      console.log(e)

      return undefined
    }
  },
  async fetch({ commit }: any) {
    const ctx = this as any
    const { backend } = ctx.$config

    const { $axios } = ctx

    const entities = await $axios.get(`${backend}/file/all`, {
      withCredentials: true,
    })

    commit('set', entities.data)
  },
  async create({ commit }: any, data: ICreateFile) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const { $axios } = ctx
      const res = await $axios.post(
        `${backend}/file`,
        {
          ...data,
          media: data.mediaId,
        },
        {
          withCredentials: true,
        }
      )

      return res.data
    } catch (e) {
      console.log(e)

      return undefined
    }
  },
  async delete({ commit }: any, id: number) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const { $axios } = ctx
      await $axios.delete(`${backend}/file/?id=${id}`, {
        withCredentials: true,
      })
    } catch (e) {
      console.log(e)
    }
  },
  async upload({ commit }: any, data: IUploadFile) {
    const ctx = this as any

    const { $axios } = ctx
    const { backend } = ctx.$config
    const url = `${backend}/upload/${data.mediaId}/${data.fileId}`
    try {
      const formData = new FormData()
      formData.append('file', data.file, data.fileName)
      formData.append('fileName', data.fileName)
      formData.append('type', data.type)

      const res = await $axios.post(url, formData, {
        withCredentials: true,
      })
      return res
    } catch (e) {
      console.log({
        url,
        e,
      })

      return undefined
    }
  },
  async fetchByProjectId({ commit }: any, projectId: number) {
    const ctx = this as any
    const { backend } = ctx.$config

    const { $axios } = ctx

    const entities = await $axios.get(
      `${backend}/file/all?projectId=${projectId}`,
      {
        withCredentials: true,
      }
    )
    commit('set', entities.data)
  },
}
const mutations = {
  set(state: State, entities: Entity[]) {
    state.entities = entities.sort((a, b) => a.id - b.id)
  },
  updateSettings(
    state: State,
    dto: {
      fileId: FileId
      settings: Partial<FileSettings>
    }
  ) {
    const entity = state.entities.find((e) => e.id === dto.fileId)
    if (entity) {
      entity.settings = {
        ...entity.settings,
        ...dto.settings,
      }
    }
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
  mountShared,
  create,
  upload,
  download,
  deleteFile,
}
