import _ from 'lodash'
import { Component } from 'vue'
import { MediaId } from '~/plugins/ProjectBuilder'
import { removeUndefines } from '~/plugins/shared'

interface BranchState {
  entities: Entity[]
}

export enum MediaType {
  image = 'image',
  video = 'video',
  audio = 'audio',
  panorama = 'panorama',
  model3d = 'model3d',
  d3 = '3d',
  pseudo3d = 'pseudo3d',
  logo = 'logo',
}

type State = BranchState

export interface ICreateMedia {
  name: string
  description?: string
  type: MediaType
  projectId: number
}
type Entity = {
  id: number

  type: MediaType

  name: string

  owner: number

  createdAt: Date

  updatedAt: Date

  deletedAt: Date

  public: boolean

  size: number

  project: number

  description: string

  order: number

  settings: {
    numberOfLines: number
    [key: string]: any
  }
}

async function mount(context: any, force: boolean = false) {
  if (force === false && context.$store.getters[`data/media/all`].length) return
  await context.$store.dispatch(`data/media/fetch`)
}

async function mountShared(context: any, projectId: number) {
  await context.$store.dispatch(`data/media/fetchByProjectId`, projectId)
}

export async function create(
  context: any,
  entity: ICreateMedia,
  effects: boolean = true
) {
  if (effects) context.$store.dispatch('App/startLoading')
  const res = await context.$store.dispatch(
    'data/media/create',
    removeUndefines(entity)
  )

  if (effects) {
    context.$store.dispatch('App/stopLoading')
    context.$store.dispatch('view/dialog/close')
  }
  return res
}

export async function deleteMedia(context: any, id: number) {
  const res = await context.$store.dispatch('data/media/delete', id)
  return res
}

export async function reorderMedia(
  context: any,
  ids: { id: number; order: number }
) {
  const res = await context.$store.dispatch('data/media/reorder', ids)
  return res
}

export async function updateSettings(
  context: any,
  dto: {
    mediaId: MediaId
    settings: {
      numberOfLines: number
    }
  }
) {
  const res = await context.$store.dispatch('data/media/updateSettings', dto)
  return res
}

const state: State = {
  entities: [],
}

const getters = {
  all(state: BranchState) {
    return state.entities
  },
  byId: (state: BranchState) => (id: number) => {
    return state.entities.find((e) => e.id === id)
  },
}

const actions = {
  async reorder(
    { commit }: any,
    idWithNewPosition: { id: number; order: number }
  ) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const { $axios } = ctx
      const res = await $axios.$patch(`${backend}/media`, idWithNewPosition, {
        withCredentials: true,
      })

      commit('reorder', idWithNewPosition)
      return true
    } catch (e) {
      return false
    }
  },

  async updateSettings(
    { commit }: any,
    dto: {
      mediaId: MediaId
      settings: {
        numberOfLines: number
      }
    }
  ) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const { $axios } = ctx

      const res = await $axios.$patch(
        `${backend}/media`,
        {
          id: dto.mediaId,
          settings: JSON.stringify(dto.settings),
        },
        {
          withCredentials: true,
        }
      )

      commit('updateSettings', dto)
      return true
    } catch (e) {
      return false
    }
  },
  async fetch({ commit }: any) {
    const ctx = this as any
    const { backend } = ctx.$config

    const { $axios } = ctx

    const entities = await $axios.get(`${backend}/media/all`, {
      withCredentials: true,
    })

    commit('set', entities.data)
  },
  async fetchByProjectId({ commit }: any, projectId: number) {
    const ctx = this as any
    const { backend } = ctx.$config

    const { $axios } = ctx

    const entities = await $axios.get(
      `${backend}/media/all?projectId=${projectId}`,
      {
        withCredentials: true,
      }
    )
    commit('set', entities.data)
  },
  async create({ commit }, data: ICreateMedia) {
    try {
      const ctx = this as any
      const { $axios } = ctx
      const { backend } = ctx.$config

      const res = await $axios.post(
        `${backend}/media`,
        {
          ...data,
          project: data.projectId,
        },
        {
          withCredentials: true,
        }
      )

      return res.data
    } catch (e) {
      console.error(e)

      return undefined
    }
  },
  delete({ commit }, id: number) {
    const ctx = this as any
    const { $axios } = ctx
    const { backend } = ctx.$config

    return $axios.delete(`${backend}/media/?id=${id}`, {
      withCredentials: true,
    })
  },
}
const mutations = {
  set(state: State, entities: Entity[]) {
    state.entities = entities.sort((a, b) => a.order - b.order)
  },
  reorder(state: State, idWithNewPosition: { id: number; order: number }) {
    const media = state.entities.find((m) => m.id === idWithNewPosition.id)

    if (!media) return
    media.order = idWithNewPosition.order
    state.entities = state.entities.sort((a, b) => a.order - b.order)
  },
  updateSettings(
    state: State,
    dto: {
      mediaId: MediaId
      settings: {
        numberOfLines: number
      }
    }
  ) {
    const media = state.entities.find((m) => m.id === dto.mediaId)

    if (!media) return
    media.settings = dto.settings
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
  mountShared,
  create,
}
