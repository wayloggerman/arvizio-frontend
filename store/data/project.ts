import { removeUndefines } from '~/plugins/shared'

interface BranchState {
  entities: Entity[]
}

export enum ProjectType {
  gallery = 'gallery',
  video = 'video',
  audio = 'audio',
  pano = 'pano',
  dem3 = '3d',
  pseudo3d = 'pseudo3d',
}

export interface ICreateProject {
  name: string
  description: string
  logo: File | undefined
  images: File[]
  audios: File[]
  videos: File[]
  options: any[]
  type: ProjectType
}

type State = BranchState
type Entity = {
  id: number
  name: string
  description: string
  owner: number
  type: string
  public: boolean
}

const BranchName = 'data/project'

async function mount(context: any, force: boolean = false) {
  if (force === false && context.$store.getters[`${BranchName}/all`].length)
    return
  await context.$store.dispatch(`${BranchName}/fetch`)
}
async function mountShared(context: any, projectId: number) {
  await context.$store.dispatch(`${BranchName}/fetchById`, projectId)
}

async function create(
  context: any,
  data: Partial<ICreateProject>,
  effects: boolean
): Promise<number> {
  const res = await context.$store.dispatch(
    'data/project/create',
    removeUndefines(data)
  )

  return res
}

async function deleteProject(context: any, id: number) {
  await context.$store.dispatch('data/project/delete', id)
}

async function updatePublish(context: any, id: number, publish: boolean) {
  await context.$store.dispatch('data/project/publish', {
    projectId: id,
    publish,
  })
}

function getProject(context: any, id: number) {
  return context.$store.getters['data/project/byId'](id)
}

const state: State = {
  entities: [],
}

const getters = {
  all(state: BranchState) {
    return state.entities
  },
  allByOwnerId(state: BranchState) {
    return (ownerId: number) => {
      return state.entities.filter((e) => e.owner === ownerId)
    }
  },
  byId: (state: BranchState) => (id: number) => {
    return state.entities.find((item) => item.id === id)
  },
}

const actions = {
  async fetch({ commit }: any) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config
      const res = await ctx.$axios.get(`${backend}/project`, {
        withCredentials: true,
      })

      commit('set', res.data)
      return true
    } catch (e) {
      return false
    }
  },
  async fetchById({ commit }: any, id: number) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config
      const res = await ctx.$axios.get(`${backend}/project/?id=${id}`, {
        withCredentials: true,
      })

      commit('set', res.data)
      return true
    } catch (e) {
      return false
    }
  },

  async publish(
    { commit }: any,
    dto: {
      projectId: number
      publish: boolean
    }
  ) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config
      const res = await ctx.$axios.patch(
        `${backend}/project`,
        {
          id: dto.projectId,
          public: dto.publish,
        },
        {
          withCredentials: true,
        }
      )

      commit('updatePublish', dto)

      return true
    } catch (e) {
      return false
    }
  },

  async create({ commit }: any, data: Partial<ICreateProject>) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const res = await ctx.$axios.post(`${backend}/project`, data, {
        withCredentials: true,
      })

      commit('set', [])
      return res.data
    } catch (e) {
      return undefined
    }
  },
  async delete({ commit }: any, id: number) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      await ctx.$axios.delete(`${backend}/project?id=${id}`, {
        withCredentials: true,
      })

      commit('delete', id)
      return true
    } catch (e) {
      console.log(e)
      return false
    }
  },
}
const mutations = {
  set(state: State, entities: Entity[]) {
    state.entities = entities
  },
  delete(state: State, id: number) {
    state.entities = state.entities.filter((e) => e.id !== id)
  },
  updatePublish(state: State, dto: { projectId: number; publish: boolean }) {
    const entity = state.entities.find((e) => e.id === dto.projectId)
    if (entity) entity.public = dto.publish
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
  create,
  ProjectType,
  deleteProject,
  updatePublish,
  getProject,
  mountShared,
}
