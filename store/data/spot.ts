interface BranchState {
  entities: Entity[]
}

type State = BranchState
export interface ISpot {
  id?: number

  source: { id: number }

  target: { id: number }

  x: number

  y: number

  z: number

  owner?: number
}
type Entity = ISpot
const BranchName = 'data/spot'

async function mount(context: any, force: boolean = false) {
  if (force === false && context.$store.getters[`${BranchName}/all`].length)
    return
  await context.$store.dispatch(`${BranchName}/fetch`)
}

async function mountShared(context: any, projectId: number) {
  await context.$store.dispatch(`data/spot/fetchByProjectId`, projectId)
}

async function create(context: any, spot: ISpot) {
  await context.$store.dispatch(`${BranchName}/create`, spot)
}

async function deleteSpot(context: any, id: number | undefined) {
  if (id) await context.$store.dispatch(`${BranchName}/delete`, id)
}

async function deleteBySourceAndTarget(
  context: any,
  sourceId: number,
  targetId: number
) {
  const spot = context.$store.getters[`${BranchName}/all`].find(
    (spot: ISpot) => {
      return spot.source.id === sourceId && spot.target.id === targetId
    }
  )

  if (spot) await deleteSpot(context, spot.id)
}

async function update(context: any, spot: Partial<ISpot>) {
  if (spot.id) await context.$store.dispatch(`${BranchName}/update`, spot)
}

const state: State = {
  entities: [],
}

const getters = {
  all(state: BranchState) {
    return state.entities
  },
  allByMediaId: (state: BranchState) => (id: number) => {
    return state.entities.filter((e) => e.source.id === id)
  },
}

const actions = {
  async fetch({ commit }: any) {
    const ctx = this as any
    const { backend } = ctx.$config
    const { $axios } = ctx
    const entities = await $axios.get(`${backend}/spot/all`, {
      withCredentials: true,
    })
    commit('set', entities.data)
  },
  async fetchByProjectId({ commit }: any, projectId) {
    const ctx = this as any
    const { backend } = ctx.$config
    const { $axios } = ctx
    const entities = await $axios.get(
      `${backend}/spot/all?projectId=${projectId}`,
      {
        withCredentials: true,
      }
    )
    commit('set', entities.data)
  },
  async create({ commit }, data: ISpot) {
    try {
      const ctx = this as any
      const { $axios } = ctx
      const { backend } = ctx.$config
      const res = await $axios.post(
        `${backend}/spot`,
        {
          ...data,
          z: -800,
          target: parseInt(data.target.id as unknown as string),
          source: parseInt(data.source.id as unknown as string),
        },
        {
          withCredentials: true,
        }
      )
      return true
    } catch (e) {
      console.error(e)
      return false
    }
  },
  delete({ commit }, id: number) {
    const ctx = this as any
    const { $axios } = ctx
    const { backend } = ctx.$config
    return $axios.delete(`${backend}/spot?id=${id}`, {
      withCredentials: true,
    })
  },
  update({ commit }, data: ISpot) {
    const ctx = this as any
    const { $axios } = ctx
    const { backend } = ctx.$config
    return $axios.patch(
      `${backend}/spot`,
      {
        ...data,
        z: -800,
      },
      {
        withCredentials: true,
      }
    )
  },
}
const mutations = {
  set(state: State, entities: Entity[]) {
    state.entities = entities
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
  mountShared,
  create,
  deleteSpot,
  update,
  deleteBySourceAndTarget,
}
