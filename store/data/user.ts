import { DialogMode } from '~/plugins/interfaces'

interface BranchState {
  entities: Entity[]
}

type State = BranchState
type Entity = {
  id: number
  name: string
  email: string
  createdAt: string
  updatedAt: string
}

async function mount(context: any) {
  if (context.$store.getters[`data/user/all`].length) return
  await context.$store.dispatch(`data/user/fetch`)
}

async function newPassword(context: any, email: string) {
  await context.$store.dispatch('App/loading', true)
  const res = await context.$store.dispatch('data/user/newPassword', email)

  await context.$store.dispatch('App/loading', false)

  switch (res) {
    case true:
      context.$store.dispatch('view/dialog/open', DialogMode.signIn)

      context.$store.dispatch('view/alert/setAlert', {
        type: 'success',
        message: 'Новый пароль отправлен на ваш email, проверьте почту',
      })
      return
    case false:
      context.$store.dispatch('view/alert/setAlert', {
        type: 'error',
        message: 'Пользователь с таким email не найден, либо произошла ошибка',
      })
      return
    default:
      context.$store.dispatch('view/alert/setAlert', {
        type: 'error',
        message: 'Не удалось отправить новый пароль',
      })
      return
  }
}

const state: State = {
  entities: [],
}

const getters = {
  all(state: BranchState) {
    return state.entities
  },
}

const actions = {
  async fetch({ commit }: any) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const response = await ctx.$axios.get(`${backend}/session`, {
        withCredentials: true,
      })
      commit('set', [response.data])
    } catch (e) {
      console.log(e)
    }
  },
  async logout({ commit }: any) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const response = await ctx.$axios.post(
        `${backend}/session/logout`,
        {},
        {
          withCredentials: true,
        }
      )
      commit('set', [])
      return true
    } catch (e) {
      console.log(e)
      return false
    }
  },
  async check(
    { commit }: any,
    {
      email,
      password,
    }: {
      email: string
      password: string
    }
  ) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const response = await ctx.$axios.post(
        `${backend}/session/check`,
        {
          email,
          password,
        },
        {
          withCredentials: true,
        }
      )

      if (!response.data.isConfirmed) throw new Error('not confirmed')
      commit('set', [response.data])
      return true
    } catch (e) {
      console.log(e)
      return false
    }
  },
  async newPassword({ commit }: any, email: string) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config
      const res = await ctx.$axios.get(`${backend}/user/newPassword/${email}`, {
        withCredentials: true,
      })
      return true
    } catch (e) {
      console.log(e)
      return false
    }
  },
  async create(
    { commit }: any,
    { email, password }: { email: string; password: string }
  ) {
    try {
      const ctx = this as any
      const { backend } = ctx.$config

      const response = await ctx.$axios.post(
        `${backend}/user`,
        {
          email,
          password,
        },
        {
          withCredentials: true,
        }
      )
      commit('set', [])
      return response
    } catch (e) {
      console.log(e)
      return undefined
    }
  },
}
const mutations = {
  set(state: State, entities: Entity[]) {
    state.entities = entities.filter((e) => e.id > 0)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
  newPassword,
}
