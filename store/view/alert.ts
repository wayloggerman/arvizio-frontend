import { AlertType, IAlert } from '~/components/alerts/interface'
import { AlertState } from '~/plugins/interfaces'

export function setAlert(context: any, alert: IAlert) {
  context.$store.dispatch(`view/alert/setAlert`, alert)
}

type State = AlertState

let timer: any

export const state = (): State => ({
  alert: undefined,
})

const getters = {
  alert: (state: State) => state.alert,
}

const actions = {
  setAlert({ commit }: any, alert: IAlert) {
    if (timer) clearTimeout(timer)

    commit('setAlert', alert)

    if (alert.timeout) {
      timer = setTimeout(() => {
        commit('setAlert', undefined)
      }, alert.timeout)
      return
    }

    timer = setTimeout(() => {
      commit('setAlert', undefined)
    }, 5000)
  },
}

const mutations = {
  setAlert: (state: State, alert: IAlert) => {
    state.alert = alert
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
