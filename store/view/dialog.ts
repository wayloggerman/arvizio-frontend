import { DialogMode, DialogState } from '../../plugins/interfaces'

type State = DialogState

function state(): State {
  return {
    open: false,
    mode: undefined,
  }
}

const getters = {
  open: (state: State) => state.open,
  mode: (state: State) => state.mode,
}
const mutations = {
  open(state: State, mode: DialogMode) {
    ;(state.open = true), (state.mode = mode)
  },
  close(state: State) {
    ;(state.open = false), (state.mode = undefined)
  },
  mode(state: State, mode: DialogMode) {
    state.mode = mode
  },
}
const actions = {
  open({ commit }: { commit: any }, mode: DialogMode) {
    commit('open', mode)
  },
  close({ commit }: { commit: any }) {
    commit('close')
  },
  mode({ commit }: { commit: any }, mode: DialogMode) {
    commit('mode', mode)
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}
