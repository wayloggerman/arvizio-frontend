import { IProjectSchemaBackend } from '../../../plugins/interfaces'

interface Image {
  id: number
  path: string
  name: string
}
interface BranchState {
  images: Image[]
  names: string[]
  current: number
}

type State = BranchState
const BranchName = 'view/editor/3d'

export function setImages(context: any, images: Image[]) {
  context.$store.dispatch(`${BranchName}/setImages`, images)
}

export function setNames(context: any, names: string[]) {
  context.$store.dispatch(`${BranchName}/setNames`, names)
}

export function setInx(context: any, inx: number) {
  context.$store.dispatch(`${BranchName}/setInx`, inx)
}

const state: State = {
  images: [],
  names: [],
  current: 0,
}

const getters = {
  current(state: State) {
    return state.images[state.current]
  },
  images(state: State) {
    return state.images
  },
  inx(state: State) {
    return state.current
  },
  names(state: State) {
    return state.names
  },
}

const actions = {
  setImages(context: any, images: Image[]) {
    context.commit('setImages', images)
  },
  setNames(context: any, names: string[]) {
    context.commit('setNames', names)
  },
  setInx(context: any, inx: number) {
    context.commit('setInx', inx)
  },
}
const mutations = {
  setImages(state: State, images: Image[]) {
    state.images = images
  },
  setNames(state: State, names: string[]) {
    state.names = names
  },
  setInx(state: State, inx: number) {
    state.current = inx
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  setImages,
  setNames,
  setInx,
}
