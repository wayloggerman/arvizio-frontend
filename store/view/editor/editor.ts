import {
  IProjectSchemaBackend,
  ProjectTypeBackend,
} from '../../../plugins/interfaces'

interface BranchState {
  project: IProjectSchemaBackend | undefined
}

type State = BranchState
type Entity = any

async function mount(context: any, projectId: string) {
  if (context.$store.getters[`editor/editor/project`]) return
  await context.$store.dispatch(`editor/editor/fetch`, projectId)
}

async function refreshProjects(context: any, projectId: string) {
  if (!context || !context.$store) return
  await context.$store.dispatch(`editor/editor/fetch`, projectId)
  await context.$store.dispatch(`editor/editor-3d/fetch`, projectId)
}

const state: State = {
  project: undefined,
}

const getters = {
  get(state: State) {
    return state
  },
  project(state: State) {
    return state.project
  },
  isGallery(state: State) {
    return state.project && state.project.type === ProjectTypeBackend.gallery
  },
  is360(state: State) {
    return state.project && state.project.type === ProjectTypeBackend.deg360
  },
  is3DModel(state: State) {
    return state.project && state.project.type === ProjectTypeBackend.model3d
  },
}

const actions = {
  async fetch({ commit }: any, projectId: string) {
    const ctx = this as any

    const { $axios, $config } = ctx

    const { backend } = $config

    const project: IProjectSchemaBackend = await $axios
      .get(`${backend}/projects/${projectId}`, {
        withCredentials: true,
      })
      .then((res) => res.data.data)

    commit('set', { project })
  },
  async upload({ commit, state }: any, img: string[] | string) {
    try {
      const ctx = this as any
      const { $axios, $config } = ctx
      const { backend } = $config

      const projectId = ctx.getters['editor/editor/project'].id

      const formData = new FormData()
      formData.append('id', projectId)
      if (Array.isArray(img)) img.forEach((i) => formData.append('files', i))

      formData.append('files', img as string)

      const group =
        ctx.getters['editor/is3DModel'] || ctx.getters['editor/isGallery']
          ? 'true'
          : 'false'

      const res = await $axios.put(
        `${backend}/projects/images?group=${group}`,
        formData,
        {
          withCredentials: true,
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        }
      )

      await refreshProjects(ctx, projectId)

      return true
    } catch (e) {
      console.log(e)

      return false
    }
  },
  async deleteImage({ commit }: { commit: any }, inx: number) {
    try {
      const ctx = this as any
      const { $axios, $config } = ctx
      const { backend } = $config

      const projectId = ctx.getters['editor/editor/project'].id

      await $axios.post(
        `${backend}/projects/deleteImage/`,
        {
          inx,
          projectId,
        },
        {
          withCredentials: true,
        }
      )

      await refreshProjects(ctx, projectId)
      return true
    } catch (e) {
      return false
    }
  },
}
const mutations = {
  set(state: State, newState: Partial<BranchState>) {
    Object.assign(state, newState)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
}
