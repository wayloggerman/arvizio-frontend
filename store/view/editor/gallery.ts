import { Media, MediaFile, Project } from '~/plugins/interfaces'

interface IGallaryEditor {
  projects: Project[]
  media: Media[]
  mediaFiles: MediaFile[]
  addMediaType: string | null
  addMediaId: number | null
  editMediaId: number | null
  exclusiveType: boolean
  needToRemount: boolean
  needToRemountWithoutLoad: boolean
  needToRemountPseudo3d: boolean
}

function setEditMediaId(context: any, id: number) {
  context.$store.dispatch('view/editor/gallery/setEditMediaId', id)
}

function setNeedToRemountPLayer(context: any, value: boolean) {
  context.$store.dispatch('view/editor/gallery/setRemountStatus', value)
}

function setNeedToRemountPseudo3d(context: any, value: boolean) {
  context.$store.dispatch('view/editor/gallery/setNeedToRemountPseudo3d', value)
}

function mount(context: any, projectId: number) {
  const project = context.$store.getters['data/project/all'].filter(
    (p: Project) => p.id === projectId
  )

  const media = context.$store.getters['data/media/all'].filter(
    (m: Media) => m.project.id === projectId
  )

  const mediaFiles = context.$store.getters['data/file/all'].filter(
    (file: MediaFile) =>
      media.some((media: Media) => media.id === file.media.id)
  )

  context.$store.dispatch('view/editor/gallery/setProjects', [project])
  context.$store.dispatch('view/editor/gallery/setMedia', media)
  context.$store.dispatch('view/editor/gallery/setMediaFiles', mediaFiles)
}

function sortMedia(context: any) {
  context.$store.commit('view/editor/gallery/sortMedia')
}

export function setAddMediaType(
  context: any,
  opt: {
    type: string
    mediaId?: number
    exclusiveType?: boolean
  }
) {
  context.$store.dispatch('view/editor/gallery/setAddMediaType', opt.type)
  context.$store.dispatch(
    'view/editor/gallery/setAddMediaId',
    opt.mediaId || -1
  )
  context.$store.dispatch(
    'view/editor/gallery/setExclusiveType',
    opt.exclusiveType || false
  )
}

export const state = (): IGallaryEditor => ({
  projects: [],
  media: [],
  mediaFiles: [],
  addMediaType: null,
  needToRemount: false,
  needToRemountWithoutLoad: false,
  needToRemountPseudo3d: false,
  addMediaId: -1,
  editMediaId: -1,
  exclusiveType: false,
})

export const getters = {
  project: (state: IGallaryEditor) => state.projects[0],
  media: (state: IGallaryEditor) => state.media,
  mediaFiles: (state: IGallaryEditor) => state.mediaFiles,
  addMediaType: (state: IGallaryEditor) => state.addMediaType,
  editMediaId: (state: IGallaryEditor) => state.editMediaId,
  needToRemount: (state: IGallaryEditor) => state.needToRemount,
  needToRemountWithoutLoad: (state: IGallaryEditor) =>
    state.needToRemountWithoutLoad,
  isExclusiveType: (state: IGallaryEditor) => state.exclusiveType,
  needToRemountPseudo3d: (state: IGallaryEditor) => state.needToRemountPseudo3d,
  mediaId: (state: IGallaryEditor) => state.addMediaId,
}

export const mutations = {
  setProjects(state: IGallaryEditor, projects: Project[]) {
    state.projects = projects
  },
  setMedia(state: IGallaryEditor, media: Media[]) {
    state.media = media
  },
  sortMedia(state: IGallaryEditor) {
    if (!state.media || !state.media.length) return
    state.media = state.media.sort((a: Media, b: Media) => {
      return a.order - b.order
    })
  },
  setMediaFiles(state: IGallaryEditor, mediaFiles: MediaFile[]) {
    state.mediaFiles = mediaFiles
  },
  setAddMediaType(state: IGallaryEditor, addMediaType: string | null) {
    state.addMediaType = addMediaType
  },
  setRemountStatus(state: IGallaryEditor, needToRemount: boolean) {
    state.needToRemount = needToRemount
  },
  setAddMediaId(state: IGallaryEditor, addMediaId: number) {
    state.addMediaId = addMediaId
  },
  setExclusiveType(state: IGallaryEditor, exclusiveType: boolean) {
    state.exclusiveType = exclusiveType
  },
  setEditMediaId(state: IGallaryEditor, editMediaId: number) {
    state.editMediaId = editMediaId
  },
  setNeedToRemountWithoutLoad(state: IGallaryEditor, value: boolean) {
    state.needToRemountWithoutLoad = value
  },
  setNeedToRemountPseudo3d(state: IGallaryEditor, value: boolean) {
    state.needToRemountPseudo3d = value
  },
}

export const actions = {
  setProjects({ commit }: any, projects: Project[]) {
    commit('setProjects', projects)
  },
  setMedia({ commit }: any, media: Media[]) {
    commit('setMedia', media)
  },
  setMediaFiles({ commit }: any, mediaFiles: MediaFile[]) {
    commit('setMediaFiles', mediaFiles)
  },
  setAddMediaType({ commit }: any, addMediaType: string | null) {
    commit('setAddMediaType', addMediaType)
  },
  setRemountStatus({ commit }: any, needToRemount: boolean) {
    commit('setRemountStatus', needToRemount)
  },
  setNeedToRemountWithoutLoad({ commit }: any, needToRemount: boolean) {
    commit('setNeedToRemountWithoutLoad', needToRemount)
  },
  setNeedToRemountPseudo3d({ commit }: any, needToRemount: boolean) {
    commit('setNeedToRemountPseudo3d', needToRemount)
  },
  setExclusiveType({ commit }: any, exclusiveType: boolean) {
    commit('setExclusiveType', exclusiveType)
  },
  setAddMediaId({ commit }: any, addMediaId: number) {
    commit('setAddMediaId', addMediaId)
  },
  setEditMediaId({ commit }: any, editMediaId: number) {
    commit('setEditMediaId', editMediaId)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  mount,
  sortMedia,
  setNeedToRemountPLayer,
  setNeedToRemountPseudo3d,
  setEditMediaId,
}
