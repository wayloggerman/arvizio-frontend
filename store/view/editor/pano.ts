import { ISpot } from '~/store/data/spot'

export interface Image {
  id: number
  path: string
  name: string
  spots: ISpot[]
  imagesWithSpots: string[]
  type: string
}
interface IState {
  images: Image[]

  currentModelId: number
  current: number
}

export function setSpots(context: any, spots: ISpot[][]) {
  context.$store.dispatch('view/editor/pano/setSpots', spots)
}

export function setImages(context: any, images: Image[]) {
  context.$store.dispatch('view/editor/pano/setImages', images)
}

export function prevImage(context: any) {
  context.$store.dispatch('view/editor/pano/prev')
}
export function nextImage(context: any) {
  context.$store.dispatch('view/editor/pano/next')
}

export function setInx(context: any, inx: number) {
  context.$store.dispatch('view/editor/pano/setInx', inx)
}
function correctInx(context: any) {
  const currentInx = context.$store.getters['view/editor/pano/inx']
  const images = context.$store.getters['view/editor/pano/images']
  if (currentInx >= images.length) {
    setInx(context, images.length - 1)
  }
}

export function setByMedia(context: any, media: number) {
  const images = context.$store.getters['view/editor/pano/images']

  const inx = images.findIndex((image) => image.id === media)
  if (inx === -1) return

  context.$store.dispatch('view/editor/pano/setInx', inx)
}

export const state = (): IState => ({
  images: [],
  current: 0,
  currentModelId: -1,
})

export const mutations = {
  setImages(state: IState, images: Image[]) {
    state.images = images
  },

  next(state: IState) {
    state.current++
    if (state.current >= state.images.length) state.current = 0
  },
  prev(state: IState) {
    state.current--
    if (state.current < 0) state.current = state.images.length - 1
  },
  setInx(state: IState, inx: number) {
    state.current = inx
  },
}

export const actions = {
  setImages(context: any, images: string[]) {
    context.commit('setImages', images)
  },
  nextImage(context: any) {
    context.commit('next')
  },
  prevImage(context: any) {
    context.commit('prev')
  },
  setInx(context: any, index: number) {
    context.commit('setInx', index)
  },
}

export const getters = {
  current(state: IState) {
    return state.images[state.current >= 0 ? state.current : 0]
  },
  images(state: IState) {
    return state.images
  },
  inx(state: IState) {
    return state.current
  },
  currentModelId(state: IState) {
    return state.currentModelId
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
  setImages,
  prevImage,
  nextImage,
  setSpots,
  setInx,
  setByMedia,
  correctInx,
}
