import { ProjectType } from '../data/project'

type State = {
  project: ProjectType | undefined
}

function setProjectType(context: any, type: ProjectType) {
  context.$store.dispatch('view/project/set', type)
}

const state = (): State => ({
  project: undefined,
})

const getters = {
  type(state: State) {
    return state.project
  },
}
const actions = {
  set({ commit }: any, project: ProjectType) {
    commit('set', project)
  },
}
const mutations = {
  set(state: State, project: ProjectType) {
    state.project = project
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  setProjectType,
  mutations,
}
